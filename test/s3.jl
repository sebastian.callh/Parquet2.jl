using Parquet2, Tables, Minio, AWSS3, FilePathsBase
using Parquet2: Dataset


s = Minio.Server(@__DIR__)
run(s, wait=false)

cfg = MinioConfig("http://localhost:9000", username="minioadmin", password="minioadmin")

AWSS3.AWS.global_aws_config(cfg)
